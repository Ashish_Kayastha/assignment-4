Bug 1:
Bug 1 exists because of the error in method loseBet of Punter class. The bet has already been deducted in the placeBet method but again is deducted in the loseBet method so removed the extra deduction.
Changes done:-
Folder: src/
Class: Punter
method: loseBet()

Unit test for the bug is under the PunterTest class file under the test folder. The testLoseBet() method in the PunterTest class tests the method.
Folder: test/
Unit test Class: PunterTest
method: testLoseBet()


Bug 2:
Bug 2 exists because in returnBet method of Punter class the State is set as NOT_BETTING and in the receiveWinnings method the state is checked as RECEIVING_WINNINGS. The state is set as RECEIVING_WINNING in the returnBet method now.
Changes done:-
Folder: src/
Class: Punter
method: returnBet()

Unit test for this bug is under the PunterTest class file under testReceiveWinnings() method in the test folder.
Folder: test/
Unit test Class: PunterTest
method: testReceiveWinnings()


Bug 3:
Bug 3 exists because the equals to sign was missing in the return statement balance-amount>limit under balanceExceedsLimitBy method of Punter class. The logical operator has been added now.
Changes done:-
Folder: src/
Class: Punter
method: balanceExceedsLimitBy()

Unit test for this bug is under the PunterTest class file under testbalanceExceedsLimitBy() method in the test folder.
Folder: test/
Class: PunterTest
method: testbalanceExceedsLimitBy()


Bug 4:
Bug 4 exists because the faces were checked by 1 less face. We had the code int len = faces.length - 1 under getRandom() method of Face class which has been changed removing the -1. Also in Die method roll class, the reurn value is set to face public variable and the returned.
And the BatchModeGame class is updated to find the sum of winning rounds and then this value is divided by total round count to get the win ratio.

Changes done:-
Folder: src/
Class: Die
method: roll()

Class: Face
method: getRandom()

Class: BatchModeGame
method: play() 

Unit test for this bug is under the BatchModeGameTest class file under testWinRatio() method.
Folder: test/
Class: BatchModeGameTest
method: testWinRatio()