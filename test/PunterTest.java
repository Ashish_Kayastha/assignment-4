import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PunterTest {

	Punter punter;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		String name = "Test";
		int balance = 20;
		int limit = 5;
		punter = new Punter(name, balance, limit);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testLoseBet() {
		// arrange
		int bet = 5;

		// act
		punter.placeBet(bet);
		punter.loseBet();

		// assert
		assertEquals(15, punter.getBalance());
	}

	@Test
	void testReceiveWinnings() {
		// arrange
		int bet = 5;
		int matches = 1;
		int win = matches * bet;

		// act
		punter.placeBet(bet);
		punter.returnBet();
		punter.receiveWinnings(win);

		// assert
		assertEquals(25, punter.getBalance());
	}

	@Test
	void testbalanceExceedsLimitBy() {
		// arrange
		int bet = 15;
		boolean expected = true;

		// act
		boolean actual = punter.balanceExceedsLimitBy(bet);

		// assert
		assertEquals(expected, actual);
	}
}
