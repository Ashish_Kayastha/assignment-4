import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BatchModeGameTest {

	@Mock
	Main mockMain;

	@Mock
	Die mockDice;

	@Mock
	Round mockRound;

	@Spy
	List<Die> diceList;

	@InjectMocks
	BatchModeGame batchModeGame;

	Punter punter;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		String name = "Test";
		int balance = 20000;
		int limit = 0;

		punter = new Punter(name, balance, limit);

		Die d1 = new Die();
		Die d2 = new Die();
		Die d3 = new Die();
		diceList = new ArrayList<>(Arrays.asList(d1, d2, d3));
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testWinRatio() {
		// arrange
		int numberOfGames = 10000;
		int bet = 2;
		int roundCount = 0;
		int winCount = 0;
		Face pick = null;

		// act
		while (roundCount != numberOfGames && punter.balanceExceedsLimitBy(bet)) {
			pick = Face.getRandom();
			if (Round.play(punter, diceList, pick, bet) > 0) {
				winCount++;
			}

			roundCount++;
		}

		float winRatio = (float) winCount / roundCount;

		// assert
		assertEquals(0.42, winRatio, 0.01);
	}
}
